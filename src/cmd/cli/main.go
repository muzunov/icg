package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/urfave/cli/v2"
)

// Usage container
const Usage = `
	Example usage:
	icg --interface GigabitEthernet --switch 2 --ports 35:48 --commands commands.txt --output output.txt
	icg -i GigabitEthernet -s 2 -p 35:48 -cf commands.txt -o output.txt

	switch and output parameters are optional
`

// Parameters contains all input params.
type Parameters struct {
	Interface  string
	Switch     string
	Ports      string
	Commands   string
	OutputName string
}

// Params contains the input params.
var Params Parameters

func main() {
	app := &cli.App{
		Name:        "interface commands generator",
		HelpName:    "icg",
		Version:     "1.0.0",
		Description: "Builds a list of commands based upon input params",
		UsageText:   Usage,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "interface",
				Aliases:     []string{"i"},
				Value:       "",
				Usage:       "The interface name",
				Destination: &Params.Interface,
				Required:    true,
			},
			&cli.StringFlag{
				Name:        "switch",
				Aliases:     []string{"s"},
				Value:       "0",
				Usage:       "The switch number",
				Destination: &Params.Switch,
			},
			&cli.StringFlag{
				Name:        "ports",
				Aliases:     []string{"p"},
				Value:       "",
				Required:    true,
				Usage:       "Specify a port or port range using either {port} or {start}:{end}",
				Destination: &Params.Ports,
			},
			&cli.StringFlag{
				Name:        "commands",
				Aliases:     []string{"cf"},
				Usage:       "A list of commands separated by ','",
				Destination: &Params.Commands,
				Required:    true,
			},
			&cli.StringFlag{
				Name:        "output",
				Aliases:     []string{"o"},
				Usage:       "The output file name",
				Destination: &Params.OutputName,
				Value:       "output.txt",
			},
		},
		Action: action,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func action(c *cli.Context) error {
	ports, err := getPorts(Params.Ports)
	if err != nil {
		return err
	}

	var start, end int

	if len(ports) == 1 {
		start = 0 // QUESTION: start from 0 or from 1?
		end = ports[0]
	}

	if len(ports) == 2 {
		start = ports[0]
		end = ports[1]
	}

	bytes, err := ioutil.ReadFile(Params.Commands)
	if err != nil {
		return err
	}

	commands := strings.Split(string(bytes), ",")

	f, err := os.Create(Params.OutputName)
	if err != nil {
		return err
	}
	defer f.Close()

	for i := start; i <= end; i++ {
		if _, err := f.WriteString(fmt.Sprintf("interface %v%v/0/%v\n", Params.Interface, Params.Switch, i)); err != nil {
			return err
		}

		for _, c := range commands {
			if _, err := f.WriteString(fmt.Sprintf("%v\n", c)); err != nil {
				return err
			}
		}

		if _, err := f.WriteString("!\n"); err != nil {
			return err
		}
	}

	log.Printf("Output written to: %v\n", Params.OutputName)

	return nil
}

func getPorts(portRange string) ([]int, error) {
	splitted := strings.Split(portRange, ":")

	start, err := strconv.Atoi(splitted[0])
	if err != nil {
		return nil, err
	}

	if len(splitted) == 1 {
		return []int{start}, nil
	}

	end, err := strconv.Atoi(splitted[1])
	if err != nil {
		return nil, err
	}

	return []int{start, end}, nil
}

